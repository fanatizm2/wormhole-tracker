import logging
import shelve
import urllib2

from base64 import b64encode
from os import sys
from os.path import dirname, join
from time import time
from urllib import urlencode

from bson import ObjectId as oid
from tornado.escape import json_decode, json_encode, url_escape
from tornado.gen import coroutine, Return, Task
from tornado.ioloop import IOLoop
from tornado.httpclient import AsyncHTTPClient, HTTPError, HTTPRequest
from tornado.httpserver import HTTPServer
from tornado.options import define, options, parse_command_line
from tornado.web import RequestHandler, Application
from tornado.websocket import WebSocketHandler

from auxiliaries import authenticated, Router


define('client_id')
define('client_key')
define('redirect_uri')

http_client = AsyncHTTPClient()


class App(Application):
    def __init__(self, client_id, client_key):
        routes = [
            (r"/sign", SignHandler),
            (r"/signin", SigninHandler),
            (r"/auth/(.*)", AuthHandler),
            (r"/", MainHandler),
            (r"/signout", SignoutHandler),
            (r"/poll", PollingHandler),
        ]
        settings = {
            'static_path': join(dirname(__file__), 'static'),
            'template_path': join(dirname(__file__), 'templates'),
            'cookie_secret': 'b3i2b35i23u520352352532n'
        }
        super(App, self).__init__(routes, **settings)
        self.client_id = client_id
        self.client_key = client_key
        self.vagrants = []

    @coroutine
    def authorize(self, code, refresh=False):
        """
        Purpose
            Authorization and user data update
        Process
            1. Get access token
            2. Verify access token and get character info
            3. Set or update character database data
            4. Return user id in case of success
        """

        ''' 1 '''
        authorization = "https://login.eveonline.com/oauth/token"
        credentials = b64encode(self.client_id + ':' + self.client_key)
        headers = {
            'Authorization': "Basic " + credentials,
            'Content-Type': 'application/json',
            'Host': "login.eveonline.com"
        }
        if not refresh:
            body = json_encode({
                'grant_type': 'authorization_code',
                'code': code
            })
        else:
            body = json_encode({
                'grant_type': 'refresh_token',
                'refresh_token': code
            })
        request = HTTPRequest(
            authorization,
            method="POST",
            headers=headers,
            body=body
        )
        try:
            response = yield http_client.fetch(request)
            logging.warning(response)
            logging.info(response.body)
            tokens = json_decode(response.body)
        except HTTPError as e:
            logging.error(e)
        else:
            ''' 2 '''
            verify = "https://login.eveonline.com/oauth/verify"
            headers = {
                'Authorization': 'Bearer ' + tokens['access_token'],
                'Host': 'login.eveonline.com'
            }
            request = HTTPRequest(verify, headers=headers)
            try:
                response = yield http_client.fetch(request)
            except HTTPError as e:
                logging.error(e)
            else:
                charinfo = json_decode(response.body)
                user_id = str(charinfo['CharacterID'])
                try:
                    db = shelve.open('data.db', writeback=True)
                    ''' 3 '''
                    if not db['users'].get(user_id):
                        db['users'][user_id] = charinfo
                        db['users'][user_id].update({
                            'access_token': tokens['access_token'],
                            'refresh_token': tokens['refresh_token'],
                            'router': Router()
                        })
                    else:
                        db['users'][user_id]['access_token'] = tokens['access_token']
                    db.close()
                except Exception as e:
                    logging.error(e)
                else:
                    ''' 4 '''
                    raise Return(user_id)

    @coroutine
    def character(self, user, uri, method):
        url = "https://crest-tq.eveonline.com/characters/" + str(user['CharacterID']) + uri
        headers = {
            'Authorization': 'Bearer ' + user['access_token']
        }
        request = HTTPRequest(
            url,
            headers=headers,
            method=method,
            allow_nonstandard_methods=True if method != 'GET' else False
        )
        logging.info('Fetching character related data')
        try:
            response = yield http_client.fetch(request)
        except HTTPError as e:
            logging.error(e)
            yield self.authorize(user['refresh_token'], refresh=True)
            headers['Authorization'] = 'Bearer ' + user['access_token']
            try:
                response = yield http_client.fetch(request)
            except HTTPError as e:
                logging.error(e)
            else:
                raise Return( json_decode(response.body) )
        else:
            logging.warning(response)
            raise Return( json_decode(response.body) )


class BaseHandler(RequestHandler):
    @property
    def client_id(self):
        return self.application.client_id

    @property
    def client_key(self):
        return self.application.client_key

    @property
    def vagrants(self):
        return self.application.vagrants

    @property
    def user_id(self):
        return self.get_secure_cookie("auth_cookie")

    @property
    def user(self):
        db = shelve.open('data.db', writeback=True)
        logging.info(self.user_id)
        user = db['users'].get(self.user_id)
        logging.info(user)
        db.close()
        return user or {}

    @property
    def authorize(self):
        return self.application.authorize

    @property
    def character(self):
        return self.application.character


class SignHandler(BaseHandler):
    @coroutine
    def get(self):
        self.render("sign.html")


class SigninHandler(BaseHandler):
    @coroutine
    def get(self):
        login_eveonline = "https://login.eveonline.com/oauth/authorize/?"
        query = urlencode({
            'response_type': 'code',
            'redirect_uri': 'http://' + options.redirect_uri + ':13131/auth/',
            'client_id': self.client_id,
            #'scope': 'characterBookmarksRead characterLocationRead',
            'scope': 'characterLocationRead',
            'state': str(oid())
        })
        login_eveonline += query
        self.redirect(login_eveonline)


class AuthHandler(BaseHandler):
    @coroutine
    def get(self, *args, **kwargs):
        code = self.get_argument("code")
        state = self.get_argument("state")
        user_id = yield self.authorize(code)
        if user_id:
            self.set_secure_cookie("auth_cookie", str(user_id))
        self.redirect('/')


class MainHandler(BaseHandler):
    @authenticated
    @coroutine
    def get(self, *args, **kwargs):
        kwargs = {
            'hostname': self.request.host,
            'user': self.user
        }
        self.render("main.html", **kwargs)


class SignoutHandler(RequestHandler):
    @coroutine
    def get(self, *args, **kwargs):
        self.clear_cookie("auth_cookie")
        self.redirect('/sign')


class BaseSocketHandler(WebSocketHandler):
    @property
    def client_id(self):
        return self.application.client_id

    @property
    def client_key(self):
        return self.application.client_key

    @property
    def vagrants(self):
        return self.application.vagrants

    @property
    def user_id(self):
        return self.get_secure_cookie("auth_cookie")

    @property
    def user(self):
        db = shelve.open('data.db', writeback=True)
        logging.info(self.user_id)
        user = db['users'].get(self.user_id)
        logging.info(user)
        db.close()
        return user or {}

    @property
    def authorize(self):
        return self.application.authorize

    @property
    def character(self):
        return self.application.character


class PollingHandler(BaseSocketHandler):
    @coroutine
    def run(self):
        user = self.user
        router = user['router']
        self.tracking = True
        while self.tracking:
            # Make a request to know current location
            location = yield self.character(user, '/location/', 'GET')
            if location:
                router.build_map(location['solarSystem']['name'])
                self.write_message( json_encode(router.tree) )
            yield Task(IOLoop.current().add_timeout, time() + 5)

    def open(self):
        if self.user_id:
            self.tracking = False
            self.vagrants.append(self)
            logging.info("Connection received from " + self.request.remote_ip)
        else:
            self.close()

    @coroutine
    def on_message(self, message):
        logging.info(message)
        message = json_decode(message)
        if message == 'track':
            if not self.tracking:
                yield self.run()
        elif message == 'stop':
            self.tracking = False

    def on_close(self):
        self.vagrants.remove(self)
        logging.info("Connection closed, " + self.request.remote_ip)


def main():
    parse_command_line()

    if not options.client_id or not options.client_key or not options.redirect_uri:
        logging.error("Application credentials must be provided as follows:")
        logging.warning(("python server.py"
                         "--client_id=yourid"
                         "--client_key=yourkey"
                         "--redirect_uri=youruri"))
        sys.exit(1)

    app = App(options.client_id, options.client_key)
    http_server = HTTPServer(app)
    http_server.listen(13131)

    try:
        logging.info("Starting server...")
        IOLoop.current().start()
    except (SystemExit, KeyboardInterrupt):
        logging.info("Stopping server.")
        IOLoop.current().stop()
        http_client.close()
        sys.exit()

    except Exception as e:
        logging.error(e)
        IOLoop.current().stop()
        http_client.close()
        sys.exit(1)

if __name__ == '__main__':
    main()
